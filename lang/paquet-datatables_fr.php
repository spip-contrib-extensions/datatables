<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/getID3.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'datatables_description' => 'Ce plugin permet d\'utiliser https://datatables.net/ pour mettre en forme de tableau un fichier csv.',
	'datatables_slogan' => 'Mise en forme de fichiers CSV en tableau HTML.'
);