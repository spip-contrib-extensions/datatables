<?php

function datatables_affichage_final($flux){
	// inserer le head seulement si presence d'un class='datatables'
	// il faut etre pas trop stricte car on peut avoir rel='nofollow encolsure' etc...
	if ((strpos($flux,'datatables')!==false)){
		$p = stripos($flux,"</head>");
		$ins = '<script type="text/javascript" src="'. find_in_path('js/jquery.dataTables.js') .'"></script>' . "\n" .
		'<link rel="stylesheet" type="text/css" href="'. find_in_path('css/jquery.dataTables.css') .'" />' . "\n" .
		'<link rel="stylesheet" type="text/css" href="'. find_in_path('css/datatables.css') .'" />' . "\n" ;
		if ($p)
			$flux = substr_replace($flux,$ins,$p,0);
	}
	return $flux;
}
