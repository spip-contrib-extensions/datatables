# Datatables

Ce plugin permet d'utiliser https://datatables.net/ pour mettre en forme de tableau un fichier csv.

```
<INCLURE{fond=inclure/datatable,csv=mon_fichier.csv}>
```

## Options

**Ordonner sur une colonne**

Exemple : ordonner selon la 2e colonne :

```
<INCLURE{fond=inclure/datatable,csv=demo.csv,ordonner=2}>
```


**Filtrer toutes les colonnes**

```
<INCLURE{fond=inclure/datatable,csv=demo.csv,filtrer=tout}>
```

**Filtrer une seule colonne**

Exemple : filtrer la 3e colonne :

```
<INCLURE{fond=inclure/datatable,csv=demo.csv,filtrer=3}>
```

## Modèle

Dans un article, un peut également utiliser un modèle sur un document joint.

```
<datatable1|filtrer=4|ordonner=4>
```
